# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  
  # POST /resource
  def create
    super do |user|
      Commands::Welcome.new(user:).execute
    end
  end
end
