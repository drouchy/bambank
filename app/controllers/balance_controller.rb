

class BalanceController < ApplicationController
  before_action :authenticate_user!
  
  def show
    @balance = Commands::Balance.new(user: current_user).execute
  end
end