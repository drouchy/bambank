
class Commands::Balance
  def initialize(user:)
    @user = user
  end
  
  def execute
    Operation.where(user:).sum(Money.zero, &:amount) 
  end

  private
  
  attr_reader :user
end
