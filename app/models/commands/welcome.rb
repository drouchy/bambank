
class Commands::Welcome
  def initialize(user:)
    @user = user
  end
  
  def execute
    Operation.create!(user:, amount: AMOUNT, description: 'welcome bonus', operation_type: 'welcome')
  end
  
  private
  
  attr_reader :user
  
  AMOUNT = Money.from_amount(100, 'BAM')
  private_constant :AMOUNT
end
