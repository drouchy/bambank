
class Commands::Transfer
  
  def execute(from:, to:, amount:)
    ActiveRecord::Base.transaction do
      Operation.create!(user: from, amount: amount * -1, description: "transfer to #{to.email}", operation_type: 'transfer_out')
      Operation.create!(user: to, amount:, description: "transfer from #{to.email}", operation_type: 'transfer_in')
    end
  end
  
end
