class Operation < ApplicationRecord
  belongs_to :user
  
  monetize :amount_cents
  
  validates :operation_type, presence: true, inclusion: { in: %w[welcome transfer_in transfer_out] }
  validates :description, presence: true
  
  validates :amount, presence: true
  validate :amount_positive
  validate :bam_currency
  
  private
  
  def amount_positive
    errors.add(:amount, "can't be zero") if amount.to_i.zero?
  end
  
  def bam_currency
    errors.add(:amount, "only support operation in BAM") unless amount.currency.iso_code == 'BAM'
  end
end
