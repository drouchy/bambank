# encoding : utf-8

MoneyRails.configure do |config|
  config.register_currency = {
    priority:            1,
    iso_code:            "BAM",
    name:                "Bameuros",
    symbol:              "BAM",
    symbol_first:        true,
    subunit:             "Subcent",
    subunit_to_unit:     100,
    thousands_separator: ".",
    decimal_mark:        ","
  }
  config.default_currency = :bam
  
  config.rounding_mode = BigDecimal::ROUND_HALF_EVEN
  config.locale_backend = :i18n
end
