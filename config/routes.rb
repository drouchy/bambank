Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }
  
  root "home#index"
  
  get '/balance', to: "balance#show"
end
