require "rails_helper"

RSpec.describe "User signin", type: :system do
  include Devise::Test::IntegrationHelpers
  
  let!(:user) { User.create!(email: 'user@example.com', password: 'asdfasdf')   }
  
  it "allows previously registered users to sign in" do
    visit "/"
    click_link "Log in"
    
    expect(page).to have_content("Sign in to your account")
    
    fill_in "user[email]", with: "user@example.com"
    fill_in "user[password]", with: "asdfasdf"
    click_button "Sign in"
    
    expect(page).to have_content("user@example.com")
    expect(page).to have_content("Sign Out")
  end
  
  it "allows to sign out" do
    sign_in user
    
    visit "/"
    expect(page).to have_content("user@example.com")
    
    click_link "Sign Out"
    
    expect(page).to_not have_content("user@example.com")
    expect(page).to have_content("Log in")
  end
end
