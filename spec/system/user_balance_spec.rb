require "rails_helper"

RSpec.describe "User registration", type: :system do  
  let(:user) { User.create(email: 'alice@example.com', password: 'asdfasdf') }

  before do
    create(:operation, user:, amount: Money.from_amount(1_000, 'BAM'))
    create(:operation, user:, amount: Money.from_amount(231, 'BAM'))
    
    sign_in user
  end
  
  it "it shows the user account balance" do
    visit "/balance"
    
    expect(page).to have_content 'BAM1,231'
  end
end
