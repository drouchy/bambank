require 'rails_helper'

RSpec.describe "Home page", type: 'system' do
  it "renders the home page" do
    visit "/"
    
    expect(page).to have_content("Welcome to Bambank")
  end
end