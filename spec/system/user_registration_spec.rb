require "rails_helper"

RSpec.describe "User registration", type: :system do
  it "allows users to register" do
    visit "/"
    click_link "Sign up"
    
    expect(page).to have_content("Register to Bamkbank")
    
    register
    
    expect(page).to have_content("user@example.com")
    expect(page).to have_content("Sign Out")
    
    user = User.find_by(email: 'user@example.com')
    expect(user).to_not be_nil
  end
  
  it "gives BAM 100 to the user for registering" do
    visit "/"
    click_link "Sign up"
    
    register
    
    user = User.find_by(email: 'user@example.com')
    expect(user).to_not be_nil
    expect(user.operations.count).to eq 1
    expect(user.operations.first.amount).to eq Money.from_amount(100, 'BAM')
  end
  
  def register
    fill_in "user[email]", with: "user@example.com"
    fill_in "user[password]", with: "asdfasdf"
    fill_in "user[password_confirmation]", with: "asdfasdf"
    click_button "Sign up"
    
    expect(page).to have_content 'Welcome'
  end
end
