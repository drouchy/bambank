require 'rails_helper'

RSpec.describe Money, type: :model do
  describe 'Money gem configuration' do
    it 'configures the BAM currency' do
      amount = Money.from_amount(123, 'BAM')
      
      expect(amount.currency.iso_code).to eq('BAM')
    end
    
    it 'defaults the currency to BAM' do
      expect(Money.zero.currency.iso_code).to eq('BAM')
    end
  end
end
