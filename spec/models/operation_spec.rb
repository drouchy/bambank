require 'rails_helper'

RSpec.describe Operation, type: :model do
  describe 'validation' do
    it 'validates the type' do
      expect(build(:operation, operation_type: 'welcome')).to be_valid
      expect(build(:operation, operation_type: 'transfer_in')).to be_valid
      expect(build(:operation, operation_type: 'transfer_out')).to be_valid
      
      expect(build(:operation, operation_type: '')).to_not be_valid
      expect(build(:operation, operation_type: nil)).to_not be_valid
      expect(build(:operation, operation_type: 'something else')).to_not be_valid
    end
    
    it 'validates the description' do
      expect(build(:operation, description: 'BAM 100 as a welcome bonus')).to be_valid
      
      expect(build(:operation, description: '')).to_not be_valid
      expect(build(:operation, description: nil)).to_not be_valid
    end
    
    it 'validates the amount' do
      expect(build(:operation, amount: Money.from_amount(1))).to be_valid
      
      expect(build(:operation, amount: Money.zero)).to_not be_valid
      expect(build(:operation, amount: nil)).to_not be_valid
    end
    
    it 'requires the operation to belong to a user' do
      expect { create(:operation, user: create(:user)) }.to_not raise_error
      
      expect { create(:operation, user: nil) }.to raise_error ActiveRecord::RecordInvalid
    end
  end
end
