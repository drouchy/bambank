require 'rails_helper'

RSpec.describe Commands::Transfer do
  subject { described_class.new() }
  
  let(:bob) { create(:user) }
  let(:alice) { create(:user) }
  
  
  before do
    create(:operation, user: bob, amount: Money.from_amount(100, 'BAM'))
  end
  
  describe 'execute' do
    let(:amount) { Money.from_amount(50, 'BAM') }
    
    it 'creates an operation for the destinatery' do
      subject.execute(from: bob, to: alice, amount:)
      
      expect(alice.operations.count).to eq(1)
      transfer = alice.operations.last
      expect(transfer.amount).to eq(amount)
      expect(transfer.operation_type).to eq('transfer_in')
    end
    
    it 'creates an operation for the sender' do
      subject.execute(from: bob, to: alice, amount:)
      
      expect(bob.operations.count).to eq(2)
      transfer = bob.operations.order(:created_at).last
      expect(transfer.amount).to eq(amount * -1)
      expect(transfer.operation_type).to eq('transfer_out')
    end
    
  end
end