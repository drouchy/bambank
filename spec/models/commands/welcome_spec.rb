require 'rails_helper'

RSpec.describe Commands::Welcome do
  subject { described_class.new(user:) }
  
  let(:user) { create(:user) }
  
  describe 'execute' do
    it 'executes an welcome operation' do
      expect { subject.execute }.to change(Operation, :count).from(0).to(1)
      
      operation = Operation.first
      expect(operation.operation_type).to eq 'welcome'
      expect(operation.amount).to eq Money.from_amount(100, 'BAM')
      expect(operation.user).to eq user
      expect(operation.description).to include 'welcome'
    end
  end
end