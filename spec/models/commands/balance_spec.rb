require 'rails_helper'

RSpec.describe Commands::Balance do
  subject { described_class.new(user:) }
  
  let(:user) { create(:user) }
  
  describe 'execute' do
    it 'calculates the balance based on the operations' do
      create(:operation, user:, amount: Money.from_amount(12, 'BAM'))
      create(:operation, user:, amount: Money.from_amount(-120, 'BAM'))
      create(:operation, user:, amount: Money.from_amount(200, 'BAM'))
      
      expect(subject.execute).to eq Money.from_amount(92, 'BAM')
    end
    
    it 'calculates a zero balance if there is no operations' do
      expect(subject.execute).to eq Money.zero
    end
  end
end