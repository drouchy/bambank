FactoryBot.define do
  factory :operation do
    operation_type  { 'welcome' }
    amount          { Money.from_amount(100) }
    description     { 'Welcome to Bambank bonus' }
    user            { build(:user) }
  end
end