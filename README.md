# INTRODUCTION

Given the 2h time limit, I could not implement the UI for the money transfer, but with the backend code ready, implementing the happy path should not take long.

This is a vanilla Rails project, and I tried to keep the dependencies to a minimum.

# Assumption

The project mentions building a bank. My assumption is that it's important to keep some kind of ledger for all the operations executed in the system, and not just keep track of the users' balance.

The bank currency is Bambeuros, and for now we can only use that one. But for a bank currencies, currency conversions are really important. Even though we don't have to support it for this exercise, using a library like Money would really help in future development.

# dependencies

Despite the rails dependencies, I've added:

* devise: to manage user registration and authentication
* money-rails: to help with the money aspect

* rspec: this is the test framework I've been using for a long time
* factory-bot: helps with the fixtures. I've also been using for a little while

I've kept the database to sqlite, but a real production application would use Postgres or MySQL.

# Requirement

* ruby 3.2
* bundler

# Architecture

Rails provides a MVC architecture for the application. I've added a command pattern to execute the business logic.

* Commands::Balance to calculate the use balance
* Commands::Transfer to execute a transfer from one user to an other
* Commands::Welcome to give the BAM 100 to new users

I copied a couple of templates from https://tailwindui.com for the UI

# Model

* User: is provider by devise
* Operation: represents a bank operation executed on the system. For example, a welcome bonus is triggered by inserting an operation. In hindsight, the name `operation` is not great, as it's too generic, I should have named it BankOperation

To calculate the user balance, we need to sum all the user operations. This is not scalable - although that could work for quite some time - but it allows to generate the user history extremely easily. As the project is to build a bank, I think having a record of all the operations in the system is really important.

# Development

```
bundle install

./bin/dev
```

the app is accessible from http://localhost:3000


# If I had more time

* finish the transfer UI. Mainly using a transfers controller

```
resources :transfer, only: %i[new create]
```
* the commands are in the models directory. They should be moved into the app directory
* may be use a configuration to enable/disable the welcome bonus. So we could disable it without any code change
* at the moment, registering is free, but gives the user some money. It would be easy for anybody to create thousands of account, and transfer all the welcome bonuses to a single account. To solve that problem, we could only give the bonus after some sort of user identity verification. And also limit the number of bonus we distribute
* I'm not sure I would have a balance controller and view. But that was the fastest implementation to execute the code, considering the time constraint.
* the system spec are running within Chrome. I would configure to run headless by default