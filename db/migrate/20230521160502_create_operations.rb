class CreateOperations < ActiveRecord::Migration[7.0]
  def change
    create_table :operations do |t|
      t.string     :operation_type
      t.monetize   :amount
      t.text       :description
      t.references :user, null: false, foreign_key: true, index: true, null: false

      t.timestamps
    end
  end
end
